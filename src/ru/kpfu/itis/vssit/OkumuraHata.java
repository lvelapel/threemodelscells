package ru.kpfu.itis.vssit;

import java.util.Scanner;

import static java.lang.Math.*;

public class OkumuraHata extends Model {
    private double f;
    private double h_BS;
    private double h_MS;
    private int K_0 = -1;
    private double K_1;
    private double K_2;
    private double a_h_MS;
    private double L;

    public double getRadius () {
        double radius = 0;

        fillVariables();

        // Временная переменная, куда будет записана
        // преобразованная формула для нахождения  lg(d) = bufferVariable
        double bufferVariable = (L + K_1 + K_2 * log10(f) - 13.82 * log10(h_BS) - a_h_MS + K_0) /
                (-1 * (44.9 - 6.55 * log10(h_BS)));
        radius = pow(10, bufferVariable);

        System.out.println("Double radius: " + radius);
        System.out.println("Int radius: " + (int) radius);
        return radius;
    }


    protected void fillVariables () {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите L в лБ:");
        L = scanner.nextDouble();

        do {
            System.out.println("Введите f в Гц :");
            System.out.println("в пределах (f >= 150Мгц && f <= 1000Мгц) или (f >= 1500Мгц && f <= 2000Мгц)");
            f = scanner.nextDouble() / 1000000; // в МГц
        } while (!(f >= 150 && f <= 1000) || !(f >= 1500 && f <= 2000));

        System.out.println("Введите h_BS в м:");
        h_BS = scanner.nextDouble();

        System.out.println("Введите h_MS в м:");
        h_MS = scanner.nextDouble();

        do {
            System.out.println("Введите K_0 в дБ (0 или 3):");
            K_0 = scanner.nextInt();
        } while (K_0 != 0 || K_0 != 3);

        if (f >= 150 && f <= 1000) {
            K_1 = 69.55;
            K_2 = 26.16;
        } else if (f >= 1500 && f <= 2000) {
            K_1 = 69.55;
            K_2 = 26.16;
        }

        if (K_0 == 0) {
            a_h_MS = floor(1.1 * log10(f) - 0.7) * h_MS - floor(1.56 * log10(f) - 0.8);
        } else if (K_0 == 3) {
            a_h_MS = 3.2 * pow(floor(log10(11.75 * h_MS)), 2) - 4.97;

        }


    }
}
