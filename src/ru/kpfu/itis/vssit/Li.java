package ru.kpfu.itis.vssit;
import java.util.Scanner;

import static java.lang.Math.log10;
import static java.lang.Math.pow;

public class Li extends Model {
    /**
     * Переменные в формуле
     */

    private double P_rec;
    private double P_0;
    private double gamma;
    private double d_0;
    private double n;
    private double f;
    private double f_0;
    private double h_BSeff;
    private double h_MS;
    private double u;
    private double P_T;
    private double G_T;
    private double G_R;


    @Override
    public double getRadius () {
        double radius = 0;

        fillVariables();

        double F_1 = h_BSeff * h_BSeff / 30.48;
        double F_2 = pow(h_MS, u) / 3;
        double F_3 = P_T / 10;
        double F_4 = G_T / 4;
        double F_5 = G_R / 4;
        double F_0 = F_1 * F_2 * F_3 * F_4 * F_5;

        // Временная переменная, куда будет записана
        // преобразованная формула для нахождения  lg(d/d_0) = bufferVariable
        double bufferVariable = (P_rec - P_0 + n * log10(f / f_0) - F_0)
                / (-1 * gamma);
        radius = pow(10, bufferVariable) * d_0;


        System.out.println("F_1: " + F_1);
        System.out.println("F_2: " + F_2);
        System.out.println("F_3: " + F_3);
        System.out.println("F_4: " + F_4);
        System.out.println("F_5: " + F_5);
        System.out.println("F_0: " + F_0);
        System.out.println("bufferVariable: " + bufferVariable);
        System.out.println("Double radius: " + radius);
        System.out.println("Int radius: " + (int) radius);
        return radius;
    }

    protected void fillVariables () {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите P_rec в dB");
        P_rec = scanner.nextDouble();

        System.out.println("Введите P_0 в dB");
        P_0 = scanner.nextDouble();

        System.out.println("Введите gamma");
        gamma = scanner.nextDouble();

        System.out.println("Введите d_0 в метрах");
        d_0 = scanner.nextDouble();

        System.out.println("Введите n");
        n = scanner.nextDouble();

        System.out.println("Введите f в Гц");
        f = scanner.nextDouble();

        System.out.println("Введите f_0 в Гц");
        f_0 = scanner.nextDouble();

        System.out.println("Введите h_BSeff в метрах");
        h_BSeff = scanner.nextDouble();

        System.out.println("Введите h_MS в метрах");
        h_MS = scanner.nextDouble();

        System.out.println("Введите u");
        u = scanner.nextDouble();

        System.out.println("Введите P_T в Вт");
        P_T = scanner.nextDouble();

        System.out.println("Введите G_T в дБ");
        G_T = scanner.nextDouble();

        System.out.println("Введите G_R в дБ");
        G_R = scanner.nextDouble();

        scanner.close();
    }
}
