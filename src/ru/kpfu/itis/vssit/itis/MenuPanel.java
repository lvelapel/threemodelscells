package ru.kpfu.itis.vssit.itis;

import ru.kpfu.itis.vssit.Ikegami;
import ru.kpfu.itis.vssit.LOS;
import ru.kpfu.itis.vssit.Li;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class MenuPanel extends JFrame {
    View view;

    public class View {
        public JLabel lbl_height;
        public JLabel lbl_width;
        public JLabel lbl_model;
        private JCheckBox losCheckBox;
        private JCheckBox ikegamiCheckBox;
        private JCheckBox liCheckBox;
        public JTextField tf_height;
        public JTextField tf_width;
        public JTextField tf_radius;
        public JButton btn_go;

        View () {
            lbl_height = new JLabel("Высота зоны:");
            lbl_width = new JLabel("Ширина зоны:");
            lbl_model = new JLabel("Выберите модель (ввод данных в консоли)");
            tf_height = new JTextField();
            tf_width = new JTextField();
            tf_radius = new JTextField();
            btn_go = new JButton("GO!");
            losCheckBox = new JCheckBox("LOS");
            ikegamiCheckBox = new JCheckBox("Ikegami");
            liCheckBox = new JCheckBox("Li");

        }

    }

    MenuPanel () {
        super();
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize(500, 500);
        this.setLayout(new GridLayout(10, 1));
        view = new View();
        view.btn_go.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed ( ActionEvent e ) {
                try {
                    if (view.liCheckBox.isSelected()) {
                        Li li = new Li();
                        new ResultPanel(new Integer(view.tf_height.getText()), new Integer(view.tf_width.getText()),
                                (int) li.getRadius());
                    }
                    else if (view.losCheckBox.isSelected()) {
                        LOS los = new LOS();
                        new ResultPanel(new Integer(view.tf_height.getText()), new Integer(view.tf_width.getText()),
                                (int) los.getRadius());
                    }
                    else if (view.ikegamiCheckBox.isSelected()) {
                        Ikegami ikegami = new Ikegami();

                        new ResultPanel(new Integer(view.tf_height.getText()), new Integer(view.tf_width.getText()),
                                (int) ikegami.getRadius());
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(Main.menuPanel, "ERROR", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        //adding
        this.add(view.lbl_height);
        this.add(view.tf_height);
        this.add(view.lbl_width);
        this.add(view.tf_width);
        this.add(view.lbl_model);
        this.add(view.losCheckBox);
        this.add(view.liCheckBox);
        this.add(view.ikegamiCheckBox);
        this.add(view.btn_go);


        this.setVisible(true);
//        System.out.println(this.getBackground().toString());
    }
}
