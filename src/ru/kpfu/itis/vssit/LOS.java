package ru.kpfu.itis.vssit;

import java.util.Scanner;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

public class LOS extends Model {
    /**
     * Переменные в формуле
     */

    private double P_rec;
    private double K_rec;
    private double P_trans;
    private double K_trans;
    private double L;
    private double lambda;
    private double gamma;


    @Override
    public double getRadius () {
        double radius = 0;

        fillVariables();

        // Временная переменная, куда будет записана
        // преобразованная формула для нахождения   bufferVariable
        double bufferVariable = (P_trans * K_trans * K_rec * L * lambda * lambda) / (16 * PI * PI * P_rec);
        radius = pow(bufferVariable, (1 / gamma));

        System.out.println("Double radius: " + radius);
        System.out.println("Int radius: " + (int) radius);

        return radius;
    }

    @Override
    protected void fillVariables () {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите P_rec в Вт:");
        P_rec = scanner.nextDouble();

        System.out.println("Введите K_rec в разах:");
        K_rec = scanner.nextDouble();

        System.out.println("Введите P_trans в Вт:");
        P_trans = scanner.nextDouble();

        System.out.println("Введите K_trans в разах:");
        K_trans = scanner.nextDouble();

        System.out.println("Введите L:");
        L = scanner.nextDouble();

        System.out.println("Введите lambda в метрах:");
        lambda = scanner.nextDouble();

        System.out.println("Введите gamma:");
        gamma = scanner.nextDouble();

        scanner.close();
    }
}
