package ru.kpfu.itis.vssit;

import java.util.Scanner;

import static java.lang.Math.*;


public class Ikegami extends Model {

    private double L_0;
    private double f;
    private double w;
    private double h_d1;
    private double h_m;
    private double phi;
    private double K_a;
    private double K_d;
    private double K_f;
    private double d;
    private double h_1;
    private double L_p;
    private double L_c;

    @Override
    public double getRadius () {
        double radius = 0;

        fillVariables();
        /**  Временная переменная, куда будет записана
         * преобразованная формула для нахождения bufferVariable
         */
        double rightPartOfExpression = 32.4 - 16.9 - 10 * log10(w) + 10 * log10(f * (h_d1 - h_m)) +
                L_p + L_c + K_a + K_f * log10(f) - 9 * log10(d);
        double bufferVariable = L_0 - rightPartOfExpression - 20 * log10(f);
        radius = pow(10, bufferVariable / (20 + K_d));


        System.out.println("rightPartOfExpression: " + rightPartOfExpression);
        System.out.println("bufferVariable: " + bufferVariable);
        System.out.println("Double radius: " + radius);
        System.out.println("Int radius: " + (int) radius);
        return radius;
    }

    @Override
    protected void fillVariables () {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите f в Гц:");
        f = scanner.nextDouble();

        System.out.println("Введите L_0 в Гц:");
        L_0 = scanner.nextDouble();

        System.out.println("Введите w в метрах:");
        w = scanner.nextDouble();

        System.out.println("Введите h_d1 в метрах:");
        h_d1 = scanner.nextDouble();

        System.out.println("Введите h_m в метрах:");
        h_m = scanner.nextDouble();

        System.out.println("Введите phi в градусах:");
        phi = scanner.nextDouble();

        System.out.println("Введите K_a в разах:");
        K_a = scanner.nextDouble();

        System.out.println("Введите K_d в разах:");
        K_d = scanner.nextDouble();

        System.out.println("Введите K_f в разах:");
        K_f = scanner.nextDouble();

        System.out.println("Введите d в метрах:");
        d = scanner.nextDouble();

        System.out.println("Введите h_1 в метрах:");
        h_1 = scanner.nextDouble();

        if (phi >= 0 && phi <= 35)
            L_p = 10 * 0.354 * phi;
        else if (phi >= 35 && phi <= 55)
            L_p = 2.5 + 0.075 * (phi - 35);
        else if (phi >= 55 && phi <= 90)
            L_p = 4 + 0.014 * (phi - 55);

        if (h_1 > h_d1)
            L_c = 18 * log10(1 + (h_1 - h_d1));
        else L_c = 0;


        scanner.close();
    }
}
